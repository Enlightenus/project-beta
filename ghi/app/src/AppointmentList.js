import React from 'react';
import { useEffect, useState } from "react";

const AppointmentList = () => {
    const [appointments, setAppointments] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8080/api/appointments/')
            .then(response => response.json())
            .then(data => {
                setAppointments(data.appointments);
            })
            .catch(e => console.log('error: ', e));
    }, [])

    const cancelAppointment = async (id) => {

        const deleteFunctionUrl = `http://localhost:8080/api/appointments/${id}/`
        const options = {
            method: 'DELETE',
            headers: {
                "Content-Type": "application/json"
            }

        }
        const response = await fetch(deleteFunctionUrl, options)
        if (response.ok) {
            window.location.reload()
        }
    }

    const finishAppointment = async (id) => {

        const finishFunctionUrl = `http://localhost:8080/api/appointments/${id}/`
        const options = {
            method: 'PUT',
            body: JSON.stringify({ finished: true }),
            headers: { 'Content-Type': 'application/json' }

        }
        console.log(finishFunctionUrl)
        const response = await fetch(finishFunctionUrl, options)
        if (response.ok) {
            window.location.reload()
        }
    }


    return (
        <>
            <h1>List of Appointments</h1>
            <table className="table table-dark table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Reason</th>
                        <th>VIP</th>
                        <th>Assignee</th>
                        <th>Finished</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.customer_name}</td>
                                <td>{appointment.date}</td>
                                <td>{appointment.time}</td>
                                <td>{appointment.reason}</td>
                                <td>{appointment.vip && <button className="btn btn-warning"></button>}</td>
                                <td>{appointment.technician}</td>
                                <td>{appointment.finished && <p>Complete!</p>}</td>
                                <td>{!appointment.finished && <button className="btn btn-success"
                                    onClick={() => finishAppointment(appointment.id)}
                                >Finish</button>}{!appointment.finished && <button className="btn btn-danger"
                                    onClick={() => cancelAppointment(appointment.id)}
                                >Cancel</button>}</td>
                                <td></td>

                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <div>
                <a href='http://localhost:3000/appointments/new/'><button type='button'>CREATE APPOINTMENT</button></a>
            </div>
        </>
    );
}

export default AppointmentList;
