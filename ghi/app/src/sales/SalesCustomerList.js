import React from "react";


class SalesCustomerList extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            customers: [],
        };
    }
    async componentDidMount() {
        const customerUrl = "http://localhost:8090/api/salescustomer/";
        const customerResponse = await fetch(customerUrl);

        if (customerResponse.ok) {
            const customerData = await customerResponse.json();
            this.setState({customers: customerData.salescustomers});
        }
    }
    
    render() {
        return (
            <div>
                <div>
                    <h1>Dealership Customers</h1>
                    <div>
                        <table className="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Address</th>
                                    <th scope="col">Phone</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.customers.map(customer => {
                                    return (
                                        <tr>
                                            <td>{customer.name}</td>
                                            <td>{customer.address}</td>
                                            <td>{customer.phone}</td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                        <a href="http://localhost:3000/sales/customers/new/"><button type="button">NEW CUSTOMER</button></a>
                    </div>
                    <div>
                        <a href='http://localhost:3000/sales/new/'><button type='button'>NEW SALE</button></a>
                    </div>
                </div>
            </div>
        )
    }
}
export default SalesCustomerList;
