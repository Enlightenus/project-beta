import React from 'react';


class SalesCustomerForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            address: "",
            phone: "",
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeAddress = this.handleChangeAddress.bind(this);
        this.handleChangePhone = this.handleChangePhone.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const stateData = {...this.state};
        const salesCustomerUrl = "http://localhost:8090/api/salescustomer/";
        const customerFetchConfig = {
            method: "POST",
            body: JSON.stringify({...stateData}),
            headers: {
                'Content-Type': 'application/json'
            }
        };
        const customerResponse = await fetch(salesCustomerUrl, customerFetchConfig);
        if (customerResponse.ok) {
            const newCustomer = await customerResponse.json();
            console.log(newCustomer);
            const clearedState = {
                name: "",
                address: "",
                phone: "",
            }
            this.setState(clearedState);
            window.location.replace("http://localhost:3000/sales/customers/");
        }
    }

    handleChangeName(event) {
        const value = event.target.value;
        this.setState({name: value})
    }
    handleChangeAddress(event) {
        const value = event.target.value;
        this.setState({address: value})
    } 
    handleChangePhone(event) {
        const value = event.target.value;
        this.setState({phone: value})
    }   

    render() {
        return (
            <div>
                <div>
                    <h1>New Customer</h1>
                    <form onSubmit={this.handleSubmit}>
                        <div className="card">
                            <div className="card-body">
                                <label>Name</label>
                                <input onChange={this.handleChangeName} value={this.state.name} required id="name" className="form-control"/>
                            </div>
                            <div className="card-body">
                                <label>Address</label>
                                <input onChange={this.handleChangeAddress} value={this.state.address} required id="address" className="form-control"/>
                            </div>
                            <div className="card-body">
                                <label>Phone</label>
                                <input onChange={this.handleChangePhone} value={this.state.phone} required id="phone" className="form-control"/>
                            </div>
                            <button type="submit" className="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <div>
                    <a href='http://localhost:3000/sales/customers/'><button type='button'>CUSTOMER LIST</button></a>
                </div>
            </div>
        )
    }
}

export default SalesCustomerForm
