import React from 'react';


class SalesPersonList extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            salespersons: [],
        };
    }
    async componentDidMount() {
        const salespersonUrl = "http://localhost:8090/api/salesperson/";
        const salesPersonResponse = await fetch(salespersonUrl);

        if (salesPersonResponse.ok) {
            const salesPersonData = await salesPersonResponse.json();
            this.setState({salespersons: salesPersonData.salespersons});
        }
    }

    render() {
        return (
            <div>
                <div>
                    <h1>Dealership Sales People</h1>
                    <div>
                        <table className="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Employee Number</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.salespersons.map(salesperson => {
                                    return (
                                        <tr>
                                            <td>{salesperson.name}</td>
                                            <td>{salesperson.employee_number}</td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                        <a href="http://localhost:3000/sales/persons/new/"><button type="button">NEW SALESPERSON</button></a>
                    </div>
                    <div>
                        <a href='http://localhost:3000/sales/new/'><button type='button'>NEW SALE</button></a>
                    </div>
                </div>
            </div>
        )
    }
}

export default SalesPersonList
