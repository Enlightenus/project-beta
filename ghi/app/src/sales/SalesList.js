import React from 'react';


class SalesList extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            sales: [],
            term: ""
        };
        this.handleSearchTerm = this.handleSearchTerm.bind(this)
    }
    async componentDidMount() {
        const salesUrl = "http://localhost:8090/api/sale/";

        const saleResponse = await fetch(salesUrl);

        if (saleResponse.ok) {
            const saleData = await saleResponse.json();
            this.setState({sales: saleData.sales});
        }
    }

    handleSearchTerm(event) {
        let searchData = event.target.value;
        let snakeCase = searchData.charAt(0).toUpperCase() + searchData.slice(1).toLowerCase();
        this.setState({term: snakeCase})
    }

    render() {
        return (
            <div>
                <div>
                    <h1>All Sales</h1>
                    <div>
                        <div>
                            <label>Filter by Employee Name:  </label>
                            <input type="text" onChange={this.handleSearchTerm} value={this.state.term}/>
                        </div>
                        <table className="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">VIN</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Sales Person</th>
                                    <th scope="col">Customer</th>
                                </tr>
                            </thead>
                            <tbody>
                            {this.state.sales.filter(sale => {
                                if(sale.sales_person.name.includes(this.state.term)){
                                    return sale;
                                }
                            }).map(sale => {
                                return (
                                    <tr>
                                        <td>{sale.automobileVO.vin}</td>
                                        <td>{sale.price}</td>
                                        <td>{sale.sales_person.name}</td>
                                        <td>{sale.sales_customer.name}</td>
                                    </tr>
                                )
                            })}
                            </tbody>
                        </table>
                        <div>
                            <a href="http://localhost:3000/sales/new/"><button type="button">NEW SALE</button></a>
                        </div>
                        <div>
                            <a href="http://localhost:3000/sales/persons/new/"><button type="button">NEW SALESPERSON</button></a>
                        </div>
                        <div>
                            <a href="http://localhost:3000/sales/customers/new/"><button type="button">NEW CUSTOMER</button></a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default SalesList
