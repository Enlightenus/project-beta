import React from 'react';


class SaleForm extends React.Component {
    constructor (props) {
        super(props)
        this.state = {  
            price: "",
            automobileVO: "",
            automobiles: [],
            sales_person: "",
            salesPersons: [],
            sales_customer: "",
            salesCustomers: [],
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangePrice = this.handleChangePrice.bind(this);
        this.handleChangeAutomobile = this.handleChangeAutomobile.bind(this);
        this.handleChangeSalesPerson = this.handleChangeSalesPerson.bind(this);
        this.handleChangeSalesCustomer = this.handleChangeSalesCustomer.bind(this);
    }

    async componentDidMount() {
        const automobileUrl = "http://localhost:8100/api/automobiles/";
        const salesPersonUrl = "http://localhost:8090/api/salesperson/";
        const salesCustomerUrl = "http://localhost:8090/api/salescustomer/";
        
        const automobileResponse = await fetch(automobileUrl);
        const salesPersonResponse = await fetch(salesPersonUrl);
        const salesCustomerResponse = await fetch(salesCustomerUrl);
        
        if (automobileResponse.ok) {
            const automobileData = await automobileResponse.json();
            this.setState({automobiles: automobileData.autos});
        }
        if (salesPersonResponse.ok) {
            const salesPersonData = await salesPersonResponse.json();
            this.setState({salesPersons: salesPersonData.salespersons});
        }
        if (salesCustomerResponse.ok) {
            const salesCustomerData = await salesCustomerResponse.json();
            this.setState({salesCustomers: salesCustomerData.salescustomers});
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const stateData = {...this.state};
        delete stateData.automobiles;
        delete stateData.salesPersons;
        delete stateData.salesCustomers;
        const inventoryUrl = `http://localhost:8100/api/automobiles/${stateData.automobileVO}/`;
        const inventoryFetchConfig = {
            method: "PUT",
            body: JSON.stringify({'sold': true}),
            headers: {
                'Content-Type': 'application/json'
            }
        };
        const saleUrl = "http://localhost:8090/api/sale/";
        const saleFetchConfig = {
            method: "POST",
            body: JSON.stringify({...stateData}),
            headers: {
                'Content-Type': 'application/json'
            }
        };
        const inventoryResponse = await fetch(inventoryUrl, inventoryFetchConfig);
        const saleResponse = await fetch(saleUrl, saleFetchConfig);
        if (inventoryResponse.ok) {
            const saleUpdate = await inventoryResponse.json();
            console.log(saleUpdate);
        };
        if (saleResponse.ok) {
            const newSale = await saleResponse.json();
            console.log(newSale);
            const clearedState = {
                price: "",
                automobileVO: "",
                sales_person: "",
                sales_customer: ""
            }
            this.setState(clearedState);
            window.location.replace("http://localhost:3000/sales/");
        }
    }

    handleChangePrice(event) {
        const value = event.target.value;
        this.setState({price: value})
    }
    handleChangeAutomobile(event) {
        const value = event.target.value;
        this.setState({automobileVO: value})
    }
    handleChangeSalesPerson(event) {
        const value = event.target.value;
        this.setState({sales_person: value})
    }
    handleChangeSalesCustomer(event) {
        const value = event.target.value;
        this.setState({sales_customer: value})
    }
    
    render() {
        return (
            <div>
                <div>
                    <h1>New Sale</h1>
                    <form onSubmit={this.handleSubmit}>
                        <div className="card">
                            <div className="card-body">
                                <label>Price</label>
                                <input onChange={this.handleChangePrice} value={this.state.price} required id="price" className="form-control"/>
                            </div>
                            <div className="card-body">
                                <select onChange={this.handleChangeAutomobile} value={this.state.automobileVO} required id='automobileVO' className='form-select'>
                                    <option value="">Select an Automobile</option>
                                    {this.state.automobiles.filter(automobile => {
                                        if (automobile.sold === false) {
                                            return automobile;
                                        }
                                    }).map(unsoldAutomobile => {
                                        return (
                                            <option key={unsoldAutomobile.id} value={unsoldAutomobile.vin}>
                                                {unsoldAutomobile.vin}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="card-body">
                                <select onChange={this.handleChangeSalesPerson} value={this.state.sales_person} required id='sales_person' className='form-select'>
                                    <option value="">Select a Salesperson</option>
                                    {this.state.salesPersons.map(salesPerson => {
                                        return (
                                            <option key={salesPerson.id} value={salesPerson.name}>
                                                {salesPerson.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="card-body">
                                <select onChange={this.handleChangeSalesCustomer} value={this.state.sales_customer} required id='sales_customer' className='form-select'>
                                    <option value="">Select a Customer</option>
                                    {this.state.salesCustomers.map(salesCustomer => {
                                        return (
                                            <option key={salesCustomer.id} value={salesCustomer.name}>
                                                {salesCustomer.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button type="submit" className="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <div>
                    <a href='http://localhost:3000/sales/'><button type='button'>SALES LIST</button></a>
                </div>
            </div>
        )
    }
}

export default SaleForm
