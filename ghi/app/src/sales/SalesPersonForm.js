import React from 'react';


class SalesPersonForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            employee_number: "",
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeEmployeeNumber = this.handleChangeEmployeeNumber.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const stateData = {...this.state};
        const salesPersonUrl = "http://localhost:8090/api/salesperson/";
        const personFetchConfig = {
            method: "POST",
            body: JSON.stringify({...stateData}),
            headers: {
                'Content-Type': 'application/json'
            }
        };
        const personResponse = await fetch(salesPersonUrl, personFetchConfig);
        if (personResponse.ok) {
            const newPerson = await personResponse.json();
            console.log(newPerson);
            const clearedState = {
                name: "",
                employee_number: "",
            }
            this.setState(clearedState);
            window.location.replace("http://localhost:3000/sales/persons/");
        }
    }

    handleChangeName(event) {
        const value = event.target.value;
        this.setState({name: value})
    }
    handleChangeEmployeeNumber(event) {
        const value = event.target.value;
        this.setState({employee_number: value})
    }  

    render() {
        return (
            <div>
                <div>
                    <h1>New Sales Person</h1>
                    <form onSubmit={this.handleSubmit}>
                        <div className="card">
                            <div className="card-body">
                                <label>Name</label>
                                <input onChange={this.handleChangeName} value={this.state.name} required id="name" className="form-control"/>
                            </div>
                            <div className="card-body">
                                <label>Employee Number</label>
                                <input onChange={this.handleChangeEmployeeNumber} value={this.state.employee_number} required id="employee" className="form-control"/>
                            </div>
                            <button type="submit" className="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <div>
                    <a href='http://localhost:3000/sales/persons/'><button type='button'>SALES PERSON LIST</button></a>
                </div>
            </div>
        )
    }
}

export default SalesPersonForm
