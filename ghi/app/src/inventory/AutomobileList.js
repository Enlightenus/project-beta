import React from 'react';


class AutomobileList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            automobiles: [],
        };
    }
    async componentDidMount() {
        const automobileUrl = "http://localhost:8100/api/automobiles/";
        const automobileResponse = await fetch(automobileUrl);

        if (automobileResponse.ok) {
            const automobileData = await automobileResponse.json();
            this.setState({ automobiles: automobileData.autos });
        }
    }
    render() {
        return (
            <div>
                <div>
                    <h1>Automobile Inventory</h1>
                    <div>
                        <table className="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">Vin</th>
                                    <th scope="col">Model</th>
                                    <th scope="col">Manufacturer</th>
                                    <th scope="col">Year</th>
                                    <th scope="col">Color</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.automobiles.filter(auto => {
                                    if (auto.sold === false) {
                                        return auto;
                                    }
                                }).map(automobile => {
                                    return (
                                        <tr key={automobile.id}>
                                            <td>{automobile.vin}</td>
                                            <td>{automobile.model.name}</td>
                                            <td>{automobile.model.manufacturer.name}</td>
                                            <td>{automobile.year}</td>
                                            <td>{automobile.color}</td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                        <a href='http://localhost:3000/automobiles/new'><button type='button'>NEW AUTOMOBILE</button></a>
                    </div>
                </div>
            </div>
        )
    }
}
export default AutomobileList
