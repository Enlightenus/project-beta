import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import SalesPersonList from "./sales/SalesPersonList";
import SalesPersonForm from "./sales/SalesPersonForm";
import SalesCustomerList from "./sales/SalesCustomerList";
import SalesCustomerForm from "./sales/SalesCustomerForm";
import SaleForm from "./sales/SaleForm"
import SalesList from "./sales/SalesList"
import Nav from './Nav';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import AppointmentHistory from './AppointmentHistory';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import ManufacturerForm from './inventory/ManufacturerForm';
import ManufacturerList from './inventory/ManufacturerList';
import VehicleModelForm from './inventory/VehicleModelForm';
import VehicleModelList from './inventory/VehicleModelList';
import AutomobileForm from './inventory/AutomobileForm';
import AutomobileList from './inventory/AutomobileList';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers/">
            <Route index element={<ManufacturerList />} />
            <Route path="new/" element={<ManufacturerForm />} />
          </Route>
          <Route path="vehiclemodels/">
            <Route index element={<VehicleModelList />} />
            <Route path='new/' element={<VehicleModelForm />} />
          </Route>
          <Route path="automobiles/">
            <Route index element={<AutomobileList />} />
            <Route path='new/' element={<AutomobileForm />} />
          </Route>
          <Route path="technicians/">
            <Route index element={<TechnicianList />} />
            <Route path='new/' element={<TechnicianForm />} />
          </Route>
          <Route path="appointments/">
            <Route index element={<AppointmentList />} />
            <Route path='new/' element={<AppointmentForm />} />
            <Route path='history/' element={<AppointmentHistory />} />
          </Route>
          <Route path="sales/">
            <Route index element={<SalesList />} />
            <Route path="new/" element={<SaleForm />} />
            <Route path="persons/">
              <Route index element={<SalesPersonList />} />
              <Route path="new/" element={<SalesPersonForm />} />
            </Route>
            <Route path="customers/">
              <Route index element={<SalesCustomerList />} />
              <Route path="new/" element={<SalesCustomerForm />} />
            </Route>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
