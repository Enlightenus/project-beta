import React from 'react';
import { useEffect, useState } from "react";


const TechnicianList = () => {
    const [technicians, setTechnicians] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8080/api/technicians/')
            .then(response => response.json())
            .then(data => {
                setTechnicians(data.technicians);
            })
            .catch(e => console.log('error: ', e));
    }, [])

    return (
        <>
            <h1>Our Technicians Team</h1>
            <table className="table table-dark table-striped">
                <thead>
                    <tr>
                        <th>Team ID</th>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {technicians.map(technician => {
                        return (
                            <tr key={technician.tech_id}>
                                <td>{technician.tech_id}</td>
                                <td>{technician.tech_name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <div>
                <a href='http://localhost:3000/technicians/new/'><button type='button'>REGISTER TECHNICIAN</button></a>
            </div>
        </>
    );
}

export default TechnicianList;
