from django.urls import path
from .views import (
    api_list_appointments,
    api_detail_appointment,
    api_list_technicians,
    api_detail_technician,
    api_vin_appointments,
)

urlpatterns = [
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path(
        "appointments/<int:pk>/",
        api_detail_appointment,
        name="api_detail_appointment",
    ),
    path(
        "appointments/<str:vin>/",
        api_vin_appointments,
        name="api_vin_appointment",
    ),
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path("technicians/<int:id>/", api_detail_technician, name="api_detail_technician"),
]
