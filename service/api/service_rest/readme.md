# CarCar

Team:

* Brian - Sales
* Chengyun - Services

## Design

-Basic work plan  <br/>
First 2 day focus on Back-end. Third day check and start working on the front-end.
Troubleshoot and cleanup.

2022.12.06 Build the baisc structure of model and put easy Nav bar item on front-end<br/>
2022.12.07 Work on encoder and poller<br/>
2022.12.08 Poller done. AppointmentForm halfway done. Fixed the not JSON serializable issue. <br/>
Check-box worked now<br/>
To-do: Add the tech view and link.<br/>
2022.12.09 Add the Techician front-end part and Manufacturer.<br/>
To-do: Add the Appointment List.<br/>



## Service microservice

**Backend**<br />
Build a AutomobileVO model to handle the information of Automobile from the inventory-api.<br />
Create a technician model and a appointment model that handle the technician and appointment<br />
implement the PROTECT option to ensure the appointment will not be removed when technician is not available or left.<br />

**Frontend**<br />
Haven't decided wthe final pattern of the Nav bar yet.<br />
The Service part will include following components:<br />
-Add technician Form<br />
-Add Service Appointment (Maybe will make a button])<br />
-Service history will probably will be two components. Form and list.<br />

## How to Run this Application:

Put how to run steps here!


## Application Diagram

Put image or link to application diagram here. Identify your VOs here in the diagram.


## Services

This can either be a separate section with the services, their URLs, and ports listed here or you can include it in the application diagram
GHI: localhost:3000 (React Front End)


## API Documentation

Document the endpoints of your API for each of the methods you implement (GET, POST, etc..)
Provide sample success responses and sample request body data for the post requests.

You could theoretically screenshot insomnia.


## Value Objects

If you didn't identify the VOs in your diagram, then identify them here.
