from django.db import models


class AutomobileVO(models.Model):
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    model = models.CharField(max_length=100)
    sold = models.BooleanField(default=False)
    import_href = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.vin


class SalesPerson(models.Model):
    name = models.CharField(max_length=100, unique=True)
    employee_number = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return self.name


class SalesCustomer(models.Model):
    name = models.CharField(max_length=100, unique=True)
    address = models.CharField(max_length=200)
    phone = models.CharField(max_length=10)
    
    def __str__(self):
        return self.name


class Sale(models.Model):
    price = models.PositiveIntegerField()
    automobileVO = models.ForeignKey(
        AutomobileVO,
        related_name="automobile",
        on_delete=models.PROTECT,
    )
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="salesperson",
        on_delete=models.PROTECT,
    )
    sales_customer = models.ForeignKey(
        SalesCustomer,
        related_name="salescustomer",
        on_delete=models.PROTECT,
    )
