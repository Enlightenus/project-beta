# Generated by Django 4.0.3 on 2022-12-09 05:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0004_alter_automobilevo_options'),
    ]

    operations = [
        migrations.AlterField(
            model_name='automobilevo',
            name='vin',
            field=models.CharField(max_length=17, unique=True),
        ),
        migrations.AlterField(
            model_name='sale',
            name='automobileVO',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='automobile', to='sales_rest.automobilevo'),
        ),
        migrations.AlterField(
            model_name='sale',
            name='sales_customer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='salescustomer', to='sales_rest.salescustomer'),
        ),
        migrations.AlterField(
            model_name='sale',
            name='sales_person',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='salesperson', to='sales_rest.salesperson'),
        ),
    ]
