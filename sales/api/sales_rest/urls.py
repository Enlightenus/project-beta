from django.urls import path
from .views import api_list_salesperson, api_list_salescustomer, api_list_sale

urlpatterns = [
    path("salesperson/", api_list_salesperson, name="api_list_saleperson"),
    path("salescustomer/", api_list_salescustomer, name="api_list_salecustomer"),
    path("sale/", api_list_sale, name="api_list_sale"),
]
